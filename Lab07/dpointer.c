/* Ayodele Hamilton
 * CMPSC 210
 * Date: 10/19/15
 * Lab #7, Question:1
 * Honor Code:
 */

#include <stdio.h>
#define SIZE 12
double main() {
   double a[SIZE] = {5.0,3.0,6.0,7.0,2.0,1.0,9.0,8.0,10.0,4.0,11.0,0.0};
   double *b, *c;
   int i,temp;

   /* Ordinary array indexing: this you should already know how to do! */
   for (i = 0.0; i < SIZE; i++) {
     printf("%3f\n ",a[i]);
   }
   printf("\n");

   /* Using a pointer to advance through the array. We start with
      b pointing to a, then we add 4 to b each time through the loop:
   */
   b = a;
   for (i = 0; i < SIZE; i++) {
     printf("%3f\n ",*(b++)); /* "b++" will add 4 since b points to int */
   }
   printf("\n");

   /* Changing the base of the array. Here, we set the pointer to
      the END of array a, but we offset all of our indices by -11 to
      compensate. Note that "c[1]", "c[2]", etc. are all illegal
      locations under this scheme.
   */
   c = a+SIZE-1; /* c now points to the LAST element in a */
   for (i = -(SIZE-1); i <= 0.0;  i++) { /* Indices are c[-11], c[-10], ..., c[0] */
     printf("%3f\n ",c[i]);
   }
   printf("\n");

   /* Reverse elements in an array: */
   b = a;
   c = a+SIZE-1;
   for (i = 0; i < SIZE/2.0; i++) {
     temp = *b;
     *(b++) = *c;
     *(c--) = temp;
   }

   for (i = 0; i < SIZE; i++) {
     printf("%3f\n ",a[i]);
   }
   printf("\n");
}
