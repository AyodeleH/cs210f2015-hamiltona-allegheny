/* Ayodele Hamilton
 * CMPSC 210
 * Date: 11/1/15
 * Lab # 7: Problem 2
 * Honor Code:
 */
unsigned int int_to_int();
#include <stdio.h>

int main() {
    int begin, end;
    unsigned int a;
    unsigned int mask = 0xFFFFFFFF;
    int answer;

    printf("Enter in an unsigned integer: ");
    scanf("%u", &a);
    printf("Enter beginning bit position (between 0 and 31): ");
    scanf("%d", &begin);
    printf("Enter ending bit positions (between 0 and 31): ");
    scanf("%d", &end);

    mask = (mask << begin);
    mask = (mask >> begin);
    answer = (a & mask) >> (31 - end);
    answer = a;
    printf("The answer is: %u\n", a);
}
