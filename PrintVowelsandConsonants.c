/**
 * Ayodele Hamilton
 *
 * Prints out the vowels and the consonants of the ritchie.txt file
 *
 * To compile and run this on a data file named "myfile.txt", type:
 *
 *      gcc PrintVowelsandConsonants.c  -o vowelsandconsonants
 *      ./vowelsandconsonants  <  myfile.txt
 */
#include <stdio.h>
main() {
	int c;
	int vowels;
	int consonants;

	while((c = getchar()) != EOF) {
		if(c == 'a' || c == 'A') {
			vowels++;
		}
		if(c == 'e' || c == 'E') {
			vowels++;
		}
		if(c == 'i' || c == 'I') {
			vowels++;
		}
		if(c == 'o' || c == 'O') {
			vowels++;
		}
		if(c == 'u' || c == 'U') {
			vowels++;
		}
		else if(c > 'a' && c < 'z') {
			consonants++;
		}
		else if(c > 'A' && c < 'Z') {
			consonants++;
		}
	}

	printf("Vowels\n%c: %d\n", c, vowels);
	printf("Conosonants\n%c: %d\n", c, consonants);
}

