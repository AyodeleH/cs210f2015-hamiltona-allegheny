#Binomial Factorial Calculator
###By: Ayodele Hamilton
###Date: November 7, 2015


##Background:
	Binomial Factorial Calculator is a way to calculate the factorial of two given inputs. This calculation can be found when using trying to find the coefficients of a polynomial function. When an equation is raised to an extreme number of power than then that is when pascal's triangle can help or the equation of:

			______n!________
			 (n-k)! * k!

##How To Use Program:
Using Linux
	Step 1: Open up a terminal window 
	Step 2: find files for the program
	Step 3: Compile the program:
		First, compile the NASM program by typing into your terminal window "nasm -felf factorial.asm -o factorial.o".
		Second, integrate the compiled NASM program with the C program by typing "gcc -m32 factorial.o callFactorial.c -o callFact".
		Last, type in "./callFact" to run the program.
	Step 4: When inputing the values for the factorial, input the smallest number followed by the higher number for a positive result. 
