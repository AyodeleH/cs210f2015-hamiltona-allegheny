# Ayodele Hamilton    
# Lab 6, part 2
# Honor Code:The work I am submitting is a result of my own thinking and efforts.
#Purpose:To create a loop that will rotate the array, moving the first element in 
#the array the last place and shfting the array to the left one position.
	.data
	.align	2
a1:	.word	10,20,30,40	# the array to be rotated
lab1:	.asciiz	"Unrotated array: "
lab2:	.asciiz	"Rotated array: "
space:	.asciiz	" "
nl:	.asciiz	"\n"

	.text
	la	$a0,lab1	# Print label for unrotated array
	li	$v0,4
	syscall
	
	la	$a0,a1		# Print the unrotated array
	li	$a1,4
	jal	aprint
	
###########
#  Your code for rotating the array
#  goes in place of this comment block
###########
	li	$t0,0		#initialize counter
	lw	$t1,0($a0)	#temp = a0	
	la	$a0,a1		# Print the unrotated array
	
rotl:	slti	$t2,$t0,3	#while counter < 3
	beq	$t2,$zero,outside	#if false quit loop
	lw	$t2,4($a0)	#a[i] = a[i+1]
	sw	$t2,0($a0)	#a[i] = a[i+1]
	addi	$t0,$t0,1		#add 1 to the counter
	addi	$a0,$a0,4		#shifting the array over by 4 bits
	j 	rotl
	
outside:sw	$t1,0($a0)	#adding element in the first place
	
	la	$a0,lab2	# Print label for rotated array
	li	$v0,4
	syscall
	
	la	$a0,a1		# Print the rotated array
	li	$a1,4
	jal	aprint
		
	li	$v0,10		# Terminate program
	syscall	

#########################################################
############ Function aprint:
############    prints array at address a0 of length a1
#########################################################

aprint:	addi	$sp,$sp,-8	# Save $s0 and $s1 on the stack
	sw	$s0,0($sp)
	sw	$s1,4($sp)
	
	move	$s0,$a0		# Save a0 in s0--we need a0 for syscalls
	li	$s1,0		# s1 is our loop counter
	
loop:	slt	$t0,$s1,$a1	# See if we're done yet
	beq	$t0,$zero,done	# exit loop if s1 >= a1 (i.e., if counter >= array size)
	lw	$a0,0($s0)	# Print next array element:
	li	$v0,1
	syscall
	
	la	$a0,space	# Print a space after the array element:
	li	$v0,4
	syscall
	
	addi	$s0,$s0,4	# Move forward to next array value (4 bytes per int)
	addi	$s1,$s1,1	# Increment the loop counter
	j	loop
	
done:	la	$a0,nl		# Print a newline:
	li	$v0,4
	syscall
	
	lw	$s0,0($sp)	# Restore the old values of s0, s1
	lw	$s1,4($sp)
	addi	$sp,$sp,8
	
	jr	$ra		# Finished--return to caller
