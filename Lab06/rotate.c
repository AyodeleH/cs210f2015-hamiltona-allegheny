/**
 * Ayodele Hamilton
 * Lab 6, part 1
 * Other information (date, honor code, description, etc.)
 *
 */
#include <stdio.h>

/* Function prototypes: */
void aprint(int a[], int n);
void rotl(int a[], int n);

int main() {
  /* Three arrays to be rotated: */
  int a1[] = {10,20,30,40};
  int a2[] = {3};
  int a3[] = {11,12,13,14,15,16,17,18,19};

  /* Array a1: */
  printf("Unrotated array1: ");
  aprint(a1,4);
  rotl(a1,4);
  printf("Rotated array1: ");
  aprint(a1,4);
  rotl(a1,4);

  /* Array a2: */
  printf("Unrotated array2: ");
  aprint(a2,1);
  rotl(a2,1);
  printf("Rotated array2: ");
  aprint(a2,1);
  rotl(a2,1);

  /* Array a3: */
  printf("Unrotated array3: ");
  aprint(a3,9);
  rotl(a3,9);
  printf("Rotated array3: ");
  aprint(a3,9);
  rotl(a3,9);
}

/**
 * aprint(a,n): prints the elements of array a (of size n)
 *   on a single line, separated by blanks and terminating
 *   with a newline.
 */
void aprint(int a[], int n) {
  int i;

  for (i = 0; i < n; i++) {
    printf("%d ",a[i]);
  }
  printf("\n");
}

/**
 * rotl: prints out the elements of the array and moves the
 * elements one place over.
 */
void rotl(int a[], int n) {
    int i;
    int temp;
    for(i = 0; i != n-1; i++) {
        temp = a[i];        /*Takes the first number in the first place */
        a[i] = a[i+1];      /*Shifts the array one place to the right */
        a[i+1] = temp;      /*Puts the digit at the end of the array */
    }
    printf("\n");
}
