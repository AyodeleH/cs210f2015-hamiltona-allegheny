#Ayodele Hamilton
#CMPSC 210
#Lab 3
#Problem #:5
#Honor Code:The work I am submitting is a result of my own thinking and efforts.
#Purpose:To have the ascii value be converted into a number between 0 and 25.

	.data
	.align	2
letter: .ascii  "A"
	.align	2
c:	.space	4

	.text
	lw	$t0,letter	#load number into $t0 as the ascii value
	subi	$t0,$t0,65	#subtract the letter's ascii value from 65 to get position bet. 0 and 25
	sw	$t0,c		#store in c 
	li	$v0,10
	syscall			#close program
	
