#Ayodele Hamilton
#CMPSC 210
#Lab 3
#Problem #:1
#Honor Code:The work I am submitting is a result of my own thinking and efforts.
#Purpose:Calculate using as few instructions as possible


	.data
a:	.word 10
b: 	.word 20
c:	.word -30
d:	.word -40
x:	.space 4

	.text
	lw	$t0,b		#load b into $t0
	add	$t0,$t0,$t0	#add b twice for multiplying twice
	lw	$t1,c		#load c into $t1
	add	$t1,$t0,$t1	#add 2b + c
	lw	$t2,d		#load d into $t2
	sub	$t2,$t1,$t2	#subtract the answer of (2b + c) - d
	lw	$t3,a		#load a into $t3
	sub	$t3,$t3,$t2	#subtract $t2 from $t3
	sw	$t3,x		#store result in x