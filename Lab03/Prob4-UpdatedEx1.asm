#Ayodele Hamilton
#CMPSC 210
#Lab 3
#Problem #:4
#Honor Code:The work I am submitting is a result of my own thinking and efforts.
#Purpose:To simplify instructions.

# Bob Roos, Lab 3, 14 September 2015
# Example 1: creating space in memory and computing:
#      int a = 42;
#      int b = a + 17;


	.data
	.align	2	# always a good idea -- will be discussed in class
a:	.word	42	# 4 bytes of memory containing the integer 42
b:	.space	4	# 4 bytes of memory

	.text
	lw	$t0,a		# load contents of memory address a into $t0
	addi	$t0,$t0,17	# adding the constant 17 to a
	sw	$t0,b		# store result in address b
	
	li 	$v0,10
	syscall