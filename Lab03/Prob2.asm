#Ayodele Hamilton
#CMPSC 210
#Lab 3
#Problem #:2
#Honor Code:The work I am submitting is a result of my own thinking and efforts.
#Purpose:Calculate using as few instructions as possible

	.data
a:	.word 10
b:	.word -1
x:	.space 4

	.text
	lw	$t0,a		#load a int $to
	lw	$t1,b		#load b into $t1
	sub 	$t2,$t0,$t1	#subtract a-b
	add	$t2,$t2,$t2	#adding to itself
	addi	$t2,$t2,11	#adding 11 to get the result of x3
	add	$t3,$t0,$t1	#adding 10 + -1
	add	$t3,$t3,$t3	#multiplying 6x
	add	$t3,$t3,$t3	#
	addi	$t3,$t3,18	#add 18 to the number stored in $t3
	add	$t3,$t2,$t3	#adding the two answers together to get the result
	sw	$t3,x		#storing result in x 
	
