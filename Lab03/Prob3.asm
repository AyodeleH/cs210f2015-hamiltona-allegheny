#Ayodele Hamilton
#CMPSC 210
#Lab 3
#Problem #:3
#Honor Code:The work I am submitting is a result of my own thinking and efforts.
#Purpose:To add on the commands at the end of the code to break successfully

	.data
y:	.word 13
x:	.space 4

	.text
	lw	$t0,y		#load y into $t0
	add	$t0,$t0,$t0	#add 13, 7x to get result
	add	$t0,$t0,$t0	#adding 13 
	addi	$t0,$t0,39	#add 39 to ending result 
	sw	$t0,x		#store in variable x 
	
	li	$v0,10
	syscall			#close program

