/**
 * Ayodele Hamilton
 * This program was made to read in the txt file and then count the
 * amount of number of lines while showing you your txt file.
 *
 * To compile and run this on a data file named "myfile.txt", type:
 *
 *      gcc PrintLine.c  -o line
 *      ./line  <  myfile.txt
 */
#include <stdio.h>
main() {
    int c, count = 0;
    int line;
    line = 0;
    while ((c=getchar()) != EOF) {
        if (c == '\n') {
            putchar(c); /* Don't count the newline! */
            ++line;
        }
        else if (line%2 == 0) { /* Print and count */
            putchar(c);
        }

        else if (line%2 != 0) {
            putchar(c);
            count++;
        }

    }

        printf("Letters:%d\nThe number of lines is: %d\n",count, line);
}
