#include <stdio.h>

main() {

	int c;
	int lines;
	int i;

    lines = 0;
	while((c = getchar()) != EOF) {

		if(c == '\n') {
            putchar(c);
            i++;
        }
        if(lines == i) {
            putchar(c);
            lines++;
            printf("%3d\n",lines);
        }
        else if (lines%2 == 0) { /* Print and count */
            putchar(c);
        }

        else if (lines%2 != 0) {
            putchar(c);
        }
    }
}
