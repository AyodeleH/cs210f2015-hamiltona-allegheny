;Ayodele Hamilton
;Lab # 8: problem 4
;CMPSC 210
;Honor code:
;
;       #include <stdio.h>
;       int main() {
;           int r = 119;
;           int s = -32;
;           int t = 7;
;           int u = (r-(s-t)) & (s+t-r);
;           printf("r=%d,s=%d,t=%d,u=%d\n",r,s,t,u);

i:	dd	2015	; dd&  = 32-bit word (like .word in MIPS)
j:	dd	42

	extern	printf  ; We are going to use C's printf function

    section .data   ;constants go here
r:  dd  119     ;
s:  dd  -32     ;
t:  dd  7       ;
fmt:	db	"i=%d,j=%d,i+j=%d",10,0 ; db is like MIPS .ascii, but 10 = '\n',0 ='\0'
fmt1:   db  "r=%d,s=%d,t=%d,u=%d",10,0

    section .bss
u:  resb    4   ;Reserve 4 bytes for the answer

    extern printf   ;We are going to use C's printf function


    section .text
    global  main
main:
    push    ebp ;
    mov ebp,esp ;

    mov eax,[r] ;eax = 119
    mov ebx,[s] ;ebx = -32
    sub ebx,[t] ;ebx = ebx(-32) - 7
    sub eax,ebx ;eax = eax - ebx

    mov ebx,[s] ;ebx = -32
    add ebx,[t] ;ebx = ebx + 7
    sub ebx,[r] ;ebx = ebx -r

    and eax,ebx ;eax = eax and ebx
    mov [u],eax   ;u = eax

    push    dword   [u]   ;pushed last argument first
    push    dword   [t]
    push    dword   [s]
    push    dword   [r]
    push    dword   fmt1
    call    printf









